# EPOCH-Virgo2
This repository gives information how to run EPOCH simulation within the VAE23 containeron Virgo2.

## Login on VAE23 containerized submit nodes
- login on VAE23

```
ssh username@vae23.hpc.gsi.de
```

## Load EPOCH and dependencies
As for Virgo cluster, loadin EPOCH and dependencies is done via Linux modules.

```
module use /cvmfs/phelix.gsi.de/modulefiles
module load epoch/deb10_gcc/4.19.0_mpi4
```

As usual, you can add these 2 lines in your `run_file.sh` script to run EPOCH 
within VAE23 container.



